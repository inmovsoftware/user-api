<?php

namespace Inmovsoftware\UserApi\Providers;

use Illuminate\Support\ServiceProvider;
use Inmovsoftware\LoginApi\Http\Middleware\Cors;
use Inmovsoftware\LoginApi\Http\Middleware\jwtMiddleware;
class InmovTechUserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->make('Inmovsoftware\UserApi\Models\User');
        $this->app->make('Inmovsoftware\UserApi\Http\Controllers\UserController');

    }


}
