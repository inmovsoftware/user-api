<?php

namespace Inmovsoftware\UserApi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
    protected $table = "it_users";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $hidden = [
        'password',
    ];
    protected $dates = ['deleted_at'];
    protected $fillable = ['it_business_id', 'email',  'login_alt','name', 'last_name', 'phone', 'birthday', 'avatar_path', 'it_groups_users_id', 'it_profile_id', 'languaje', 'first_login', 'last_login', 'platform', 'model', 'version', 'uuid', 'token', 'date_password_change', 'recovery_code', 'it_positions_id'];


}
