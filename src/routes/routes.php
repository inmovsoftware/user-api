<?php
use Illuminate\Http\Request;

Route::middleware(['api', 'jwt', 'cors'])->group(function () {
Route::group([
    'prefix' => 'admin'
], function () {
    Route::apiResource('user', 'Inmovsoftware\UserApi\Http\Controllers\UserController');
        });
});
